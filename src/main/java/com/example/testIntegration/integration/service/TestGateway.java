package com.example.testIntegration.integration.service;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway
public interface TestGateway {
	
	@Gateway(requestChannel = "requestChannel")
	String echo(String message);

}
