package com.example.testIntegration.integration.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;

@Configuration
@EnableIntegration
@IntegrationComponentScan
@ComponentScan
public class TestBean {
	
	@Bean
	 public DirectChannel requestChannel() {
	  return new DirectChannel();
	 }

}
