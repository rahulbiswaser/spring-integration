package com.example.testIntegration.integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.testIntegration.integration.service.TestGateway;

@RestController
public class TestController {
	
	@Autowired
	TestGateway testGateway;
	
	@RequestMapping(path = "/test", method = RequestMethod.GET)
	public String getResponse() {
		String abc="Hello World";		
		return testGateway.echo(abc);
		
	}

}
