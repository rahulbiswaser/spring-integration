package com.example.testIntegration.integration.flow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;

import com.example.testIntegration.integration.service.TestBean;

@Configuration
@EnableIntegration
@IntegrationComponentScan
@ComponentScan
public class TestFlow {
	
	@Autowired
	TestBean Tb;
	
	@Bean
	 public IntegrationFlow simpleEchoFlow() {
	  return IntegrationFlows.from(Tb.requestChannel())
	    .transform((String s) -> s.toUpperCase())
	    .get();
	 }

}
